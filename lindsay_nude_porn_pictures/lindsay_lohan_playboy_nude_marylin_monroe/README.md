Nude photo of Lindsay Lohan. Very hot! Lindsay Lohan is an American actress, known for “The Parent Trap”, “Freaky Friday” and “Bobby”. Age 28. 

Lindsay Lohan nude ‘Playboy’ shoot leaked online: Homage to classic Marilyn Monroe images

In case you didn’t know what to get friends & family this Christmas 2011, how about a nude Lindsay Lohan Playboy photo spread?

Seems like the 25-year-old Lohan has always been enmeshed in controversy for one reason or another. Her Playboy spread is no different: the photos showing a naked Lindsay Lohan posing à la Marilyn Monroe have been leaked online, much to the (official) consternation of the Playboy top brass.

“Because of the interest & the Internet leak,” Hugh Hefner tweeted, “we’re releasing the Lindsay Lohan issue early.” Without any apparent consternation, Hefner added, “Lindsay Lohan was the top search name on the Internet yesterday. Her issue goes on sale late next week. Hot. Hot. Hot.”
Good-looking images

Granted, the Lindsay Lohan nude photos do look great. Those writing nasty comments about them are, in all probability, doing so because they’re envious: either they wanted her face and body or the (reported) almost $1 million she earned for the shoot. Or perhaps they’re just envious of her fame/notoriety. Or, why not, all of the above.

By the way, Lohan had already emulated the naked Marilyn Monroe for New York magazine in 2008. The difference is that the previous shoot reflected the Monroe of the early 1960s; the current one brings back the pre-stardom Monroe of the late 1940s/early 1950s.

![](lindsay_lohan_playboy_nude_marylin_monroe/Lindsay-Lohan-Naked-01.jpg]
![](lindsay_lohan_playboy_nude_marylin_monroe/Lindsay-Lohan-Naked-02.jpg]
![](lindsay_lohan_playboy_nude_marylin_monroe/Lindsay-Lohan-Naked-03.jpg]
![](lindsay_lohan_playboy_nude_marylin_monroe/Lindsay-Lohan-Naked-04.jpg]
![](lindsay_lohan_playboy_nude_marylin_monroe/Lindsay-Lohan-Naked-05.jpg]
![](lindsay_lohan_playboy_nude_marylin_monroe/Lindsay-Lohan-Naked-06.jpg]
![](lindsay_lohan_playboy_nude_marylin_monroe/Lindsay-Lohan-Naked-07.jpg]
![](lindsay_lohan_playboy_nude_marylin_monroe/Lindsay-Lohan-Naked-08.jpg]
![](lindsay_lohan_playboy_nude_marylin_monroe/Lindsay-Lohan-Naked-09.jpg]
![](lindsay_lohan_playboy_nude_marylin_monroe/Lindsay-Lohan-Naked-10.jpg]
![](lindsay_lohan_playboy_nude_marylin_monroe/Lindsay-Lohan-Naked-11.jpg]
![](lindsay_lohan_playboy_nude_marylin_monroe/Lindsay-Lohan-Naked-12.jpg]
